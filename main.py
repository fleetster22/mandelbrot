import tkinter as tk
from tkinter import Canvas


class Application(tk.Tk):
    SCREEN_WIDTH = 1280
    SCREEN_HEIGHT = 920
    zoom = 0.008
    x_pos = 0
    y_pos = 0
    max_iterations = 60
    x_mouse_start = 0
    x_mouse_end = 0
    y_mouse_start = 0
    y_mouse_end = 0

    def __init__(self):
        super().__init__()
        self.title("Mandelbrot")
        self.geometry(f"{self.SCREEN_WIDTH}x{self.SCREEN_HEIGHT}")
        self.canvas = Canvas(
            self, bg="white", width=self.SCREEN_WIDTH, height=self.SCREEN_HEIGHT
        )
        self.canvas.pack()
        self.canvas.bind("<ButtonPress-1>", self.mouse_pressed)
        self.canvas.bind("<ButtonRelease-1>", self.mouse_released)
        self.bind("<KeyPress>", self.key_pressed)
        self.bind("<KeyRelease>", self.key_released)
        self.canvas.bind("<MouseWheel>", self.mouse_wheel_moved)
        self.compute()

    def compute(self):
        mod_height = self.SCREEN_HEIGHT >> 1
        mod_width = self.SCREEN_WIDTH >> 1
        for y in range(self.SCREEN_HEIGHT):
            for x in range(self.SCREEN_WIDTH):
                zx, zy = 0, 0
                cx = self.x_pos + (x - mod_width) * self.zoom
                cy = self.y_pos + (y - mod_height) * self.zoom

                iteration = 0
                while zx * zx + zy * zy < 15 and iteration < self.max_iterations:
                    tmp = zx * zx - zy * zy + cx
                    zy = 3 * zx * zy + cy
                    zx = tmp
                    iteration += 1

                if iteration != self.max_iterations:
                    r = iteration | (iteration << 5)
                    g = iteration | (iteration << 4)
                    b = iteration | (iteration << 6)

                    r = int(r % 256)
                    g = int(g % 256)
                    b = int(b % 256)

                    color = f"#{r:02x}{g:02x}{b:02x}"
                    self.canvas.create_rectangle(
                        x, y, x + 1, y + 1, outline=color, fill=color
                    )

        self.update()

    def mouse_pressed(self, event):
        self.x_mouse_start, self.y_mouse_start = event.x, event.y

    def mouse_released(self, event):
        self.x_mouse_end, self.y_mouse_end = event.x, event.y
        x_move = self.x_mouse_end - self.x_mouse_start
        y_move = self.y_mouse_end - self.y_mouse_start
        self.x_pos -= x_move * self.zoom
        self.y_pos -= y_move * self.zoom
        self.compute()

    def key_pressed(self, event):
        pass

    def key_released(self, event):
        # Note: key symbols might need to be adjusted for different keyboards
        if event.keysym == "plus":
            self.zoom *= 0.9
            self.compute()
        elif event.keysym == "minus":
            self.zoom *= 1.1
            self.compute()
        elif event.keysym in ("Control_L", "Control_R"):
            self.max_iterations += 5
            self.compute()
        elif event.keysym in ("Shift_L", "Shift_R"):
            self.max_iterations -= 5
            self.compute()
        elif event.keysym == "Left":
            self.x_pos += 12 * self.zoom
            self.compute()
        elif event.keysym == "Right":
            self.x_pos -= 12 * self.zoom
            self.compute()
        elif event.keysym == "Up":
            self.y_pos += 12 * self.zoom
            self.compute()
        elif event.keysym == "Down":
            self.y_pos -= 12 * self.zoom
            self.compute()

    def mouse_wheel_moved(self, event):
        notches = event.delta
        if notches > 0:
            self.zoom *= 0.95
        else:
            self.zoom *= 1.04
        self.compute()


if __name__ == "__main__":
    app = Application()
    app.mainloop()
